#include "ssd.h"

extern lnh lnh_core;
extern global_memory_io gmio;



void clear_table(uint8_t str) {
    lnh_del_str_async(str);
}

void create_free_table() {
    clear_table(FREE_PAGE);
    clear_table(BUSY_PAGE);
    //mq_send(lnh_get_num(FREE_PAGE));
    for (uint32_t i; i < SSD_SIZE; ++i) {
        lnh_ins_sync(FREE_PAGE, INLINE(page_key, {.index=i, .overwriting=0}), INLINE(page_value, {.atr0=0, .atr1=0}));
    }
    lnh_sync();
    //mq_send(lnh_get_num(BUSY_PAGE));
}

void write_page() {
    lnh_get_first(FREE_PAGE);
    uint32_t index = get_key_attr(page_key, index); //(*(page_key*)&lnh_core.result.key).__struct.index;
    uint32_t overwriting_count = get_key_attr(page_key, overwriting); // get_result_key<page_key>().overwriting;
    lnh_del_sync(FREE_PAGE, lnh_core.result.key);

    lnh_ins_sync(BUSY_PAGE, INLINE(page_key, {.index=index, .overwriting=overwriting_count+1}), INLINE(page_value, {.atr0=0, .atr1=0}));
    mq_send(index);
    mq_send(overwriting_count+1);
    //mq_send(lnh_get_num(FREE_PAGE));
}

void free_page() {
    lnh_get_first(BUSY_PAGE);
    uint32_t index = get_key_attr(page_key, index);
    uint32_t overwriting_count = get_key_attr(page_key, overwriting);
    lnh_del_sync(BUSY_PAGE, lnh_core.result.key);

    lnh_ins_sync(FREE_PAGE, INLINE(page_key, {.index=index, .overwriting=overwriting_count}), INLINE(page_value, {.atr0=0, .atr1=0}));
    //mq_send(lnh_get_num(FREE_PAGE));
}

void write_batch() {
    uint32_t count = mq_receive();
    for (uint32_t i; i < count; ++i) write_page();
    lnh_sync();
    // lnh_ngr(BUSY_PAGE, INLINE(page_key, {.index=71, .overwriting=1}));
    // mq_send(get_key_attr(page_key, overwriting));
}