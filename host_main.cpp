#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <iomanip>
#ifdef _WINDOWS
#include <io.h>
#else
#include <unistd.h>
#endif


#include "experimental/xrt_device.h"
#include "experimental/xrt_kernel.h"
#include "experimental/xrt_bo.h"
#include "experimental/xrt_ini.h"

#include "gpc_defs.h"
#include "leonhardx64_xrt.h"
#include "gpc_handlers.h"


static void usage()
{
	std::cout << "usage: <xclbin> <sw_kernel>\n\n";
}


int main(int argc, char** argv) {
	unsigned int cores_count = 0;
	float LNH_CLOCKS_PER_SEC;

	__foreach_core(group, core) cores_count++;

	//Assign xclbin
	if (argc < 3) {
		usage();
		throw std::runtime_error("FAILED_TEST\nNo xclbin specified");
	}

	//Open device #0
	leonhardx64 lnh_inst = leonhardx64(0, argv[1]);
	__foreach_core(group, core)
	{
		lnh_inst.load_sw_kernel(argv[2], group, core);
	}

	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(create_free_table));
	}

	///*
	// Writing page
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(write_page));
	}

	int index = -1, overwriting_count = -1, size = -1;
	__foreach_core(group, core) {
		index = lnh_inst.gpc[group][core]->mq_receive();
	}
	std::cout << "index = " << index << std::endl;
	
	__foreach_core(group, core) {
		overwriting_count = lnh_inst.gpc[group][core]->mq_receive();
	}
	std::cout << "overwriting count = " << overwriting_count << std::endl;

	/*
	__foreach_core(group, core) {
		size = lnh_inst.gpc[group][core]->mq_receive();
	}
	std::cout << "size = " << size << std::endl;
	*/



	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(free_page));
	}
	/*
	__foreach_core(group, core) {
		size = lnh_inst.gpc[group][core]->mq_receive();
	}
	std::cout << "size = " << size << std::endl;
	*/

	//*/

	/*
	// Writing batch
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->start_async(__event__(write_batch));
	}

	uint32_t batch_size = 75; // amount of pages to write
	
	__foreach_core(group, core) {
		lnh_inst.gpc[group][core]->mq_send(batch_size);
	}
	*/

	printf("Success! returning 0.\n");
	return 0;
}
