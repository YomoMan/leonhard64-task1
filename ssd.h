#ifndef SSD_H_
#define SSD_H_

#include <stdlib.h>
#include <stdint.h>
#include <math.h>
//#include "lnh64.h"
#include "sw-kernel-lib/lib/lnh64.h"
//#include "gpc_io_swk.h"
#include "sw-kernel-lib/lib/gpc_io_swk.h"



//Макросы для формирования структур inline 
#define STRUCT(type, ...) typedef union { struct __VA_ARGS__ __struct; uint64_t bits; } type
//#define STRUCT(name) struct name: _base_uint64_t<name>
#define INLINE(type,...) (((type){__VA_ARGS__}).bits)
#define get_key_attr(type, attr) ((*(type*)&lnh_core.result.key).__struct.attr)


//Структуры данных
#define 		FREE_PAGE 	1 	// Свободная страница
#define 		BUSY_PAGE 	2 	// Занятая страница

// Константы 
#define SSD_SIZE 1048575


STRUCT(
page_key, {
        uint32_t               index        :32;	        //Поле 0 [0..31]: индекс
        uint32_t               overwriting  :32; 		//Поле 1 [32..63]: количество перезаписей
});

STRUCT(
page_value, {
        uint32_t               atr0         :32;	        // Поле 0 [0..31]
        uint32_t               atr1         :32; 	        // Поле 1 [32..63]
});

#endif

